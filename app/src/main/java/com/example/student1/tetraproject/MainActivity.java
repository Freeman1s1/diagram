package com.example.student1.tetraproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
CircleView diagram;
    float[] data={123, 345, 3342, 111, 349, 9834, 934};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        diagram=(CircleView) findViewById(R.id.diagramma);
        diagram.setData(data);
    }
}
