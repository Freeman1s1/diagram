package com.example.student1.tetraproject;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by student1 on 01.12.16.
 */

public class CircleView extends View {
    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
        float[] data;;
    float[] dataInPercent;
    public void setData(float[] array){
        data=array;
        float sum=0;
        for(float i:data){
            sum+=i;
        }
        float onePercent=sum/100;
        dataInPercent=new float[data.length];
        for(int i=0; i<data.length; i++){
            dataInPercent[i]=sum/100;
        }
        return ;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawARGB(255,0 , 0, 255);
        Paint CirclePaint=new Paint();
        CirclePaint.setColor(Color.YELLOW);
        CirclePaint.setStyle(Paint.Style.FILL);
        /*RectF rect=new RectF(100, 100, 400, 400);
        canvas.drawArc(rect, 0 , 36, true, new Paint());*/


        canvas.drawCircle(getWidth()/2, getHeight()/4, getHeight()/4, CirclePaint);
        Paint rectan=new Paint();
        int color=1;
        for(int i=0; i<getWidth(); i+=getWidth()/20){
            if(color==1){
                rectan.setColor(Color.RED);
                rectan.setStyle(Paint.Style.FILL);
                color=2;
            }else if(color==2){
                rectan.setColor(Color.GREEN);
                rectan.setStyle(Paint.Style.FILL);
                color=1;
            }
            canvas.drawRect(i, getHeight()/2 , i+getWidth()/20, getHeight(), rectan);
        }


    }
}
